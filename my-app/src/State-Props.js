import './App.css';
//import * as ReactDOM from 'react-dom';
import React, { useState, Fragment } from 'react';
import { v4 as uuidv4 } from 'uuid';

// //------Example state change-----------------
class ChildComponent extends React.Component {
    state = {
        count: 0
    };

    changeName = () => {
        this.setState((prevState) => {
            return {
                count: prevState.count + 1
            }
        });
    };
    render() {
        return (
            <div>
                <div style={{ color: "red" }}>count: {this.state.count}</div>
            </div>
        );
    }
}
class ParentComponent extends React.Component {
    constructor(props) {
        super(props);
        this.childRef = React.createRef();
    }

    handleClick = () => {
        console.log("childRef node:", this.childRef.current);
        this.childRef.current.changeName();
    };

    render() {
        return (
            <div className="App">
                <ChildComponent ref={this.childRef} />
                <button onClick={this.handleClick}>
                    Clicked
                </button>
            </div>
        );
    }
}
//export default ParentComponent


//------- Example---------------

class UserComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = { gender: "", name: "Danny" };
    }

    handleChangeGender = (event) => {
        this.setState({ gender: event.target.value });
    };

    handleChangeName = (event) => {
        this.setState({ name: event.target.value });
    };

    render() {
        let element = <h3>Hello, Mr. {this.state.name}!</h3>;
        if (this.state.gender === "female") {
            element = <h3>Hello, Mrs. {this.state.name}!</h3>;
        }
        return (
            <div>
                <div>
                    <label htmlFor="gender"> Select your gender: </label>
                    <select name="gender" onChange={this.handleChangeGender}>
                        <option value="male" selected>Male</option>
                        <option value="female">Female</option>
                    </select>
                </div>
                <div>
                    <label htmlFor="name"> Input your name: </label>
                    <input name="name" onChange={this.handleChangeName} value={this.state.name} />
                </div>
                {element}
            </div>
        );
    }
}
//export default UserComponent


//------- Example props change---------------
//The Data is passed from one component to another.
//thành phần con chỉ có thể nhận data in props nhưng kh thể sửa đổi. Example component Child không thể sửa đổi props.name 

const TodoItem = props => {
    console.log(props)
    const todo = props.todoProps
    const markComplate = props.markComplateFunc
    const deleteTodo = props.deleteTodoFunc

    const todoItemStyle = {
        textDecoration: todo.complated ? 'line-through' : 'none'
    }
    return (
        <div style={todoItemStyle}>
            <input type= "checkbox" 
                onChange={markComplate.bind(this, todo.id)} 
                checked = {todo.complated}/>
            {todo.title}
            <button onClick={deleteTodo.bind(this, todo.id)}>delete</button>
        </div>
    );
};

const AddTodo = props => {
    console.log(props)

    const addTodo = props.addTodoFunc
    const [title, setTitle] = useState('')

    const changeTitle = event => {
        setTitle(event.target.value)
    }
    const addSingeTodo = event => {
        event.preventDefault()
        if(title !==''){
            addTodo(title)
            setTitle('')
        }
    }
    return (
        <form onSubmit={addSingeTodo}>
            <input type='text' name='title'
                onChange={changeTitle}
                value={title}/>
            <input type='submit'/>
        </form>
    )
}

const Todos = () => {
    const [todoState, setTodoState] = useState([
        {
            id: uuidv4(),
            title: 'viec 1',
            complated: false
        },
        {
            id: uuidv4(),
            title: 'viec 2',
            complated: false
        },
        {
            id: uuidv4(),
            title: 'viec 3',
            complated: false
        }
    ])

    const markComplate = id => {
        const newTodos = todoState.map(todo => {
            if(todo.id === id) todo.complated = !todo.complated
            return todo
        })
        setTodoState(newTodos)
    }

    const deleteTodo = id => {
        const newTodos = todoState.filter(todo => todo.id !== id)
        setTodoState(newTodos)
    }
    const addTodo = title => {
        const newTodos = [...todoState, {
            id: uuidv4(),
            title,
            complated: false
        }]
        setTodoState(newTodos)
    }
    console.log('heoo')
    return (
        <Fragment>
            <AddTodo addTodoFunc={addTodo}/>
            {todoState.map(todo => {
                return <TodoItem key={todo.id} 
                    todoProps={todo} 
                    markComplateFunc={markComplate}
                    deleteTodoFunc={deleteTodo}
                    />
            })}
        </Fragment>
    );
}


export default Todos;