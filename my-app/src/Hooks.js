import "./App.css";
//import * as ReactDOM from 'react-dom';
import React, { useRef, useState, useEffect, useMemo, useReducer } from "react";

//------------------Example clock app-------------------------
class Appdemo extends React.Component {
  constructor(props) {
    super(props);
    this.state = { date: new Date() };
  }
  componentDidMount() {
    this.timerID = setInterval(() => this.tick(), 1000);
  }
  componentWillUnmount() {
    clearInterval(this.timerID);
  }
  tick() {
    this.setState({
      date: new Date(),
    });
  }
  render() {
    return (
      <div>
        <h1>Hello, world!</h1>
        <h2>It is {this.state.date.toLocaleTimeString()}.</h2>
      </div>
    );
  }
}

//------------ Example stateful components---------
// class App extends React.Component {

//   constructor() {
//     super();
//     this.state = {
//       first_name: 'Shruti',
//       last_name: 'Priya'
//     }
//   }

//   render() {
//     return (
//       <div>
//         <p> Class Component </p>
//         <p>{this.state.first_name}</p>
//         <p>{this.state.last_name}</p>
//       </div>
//     )
//   }
// }

// export default App;

//------------ HOOKS---------
//Basic Hooks: useState, useEffect

//----------Example useEffect-------------
//1. useEffect(callback)
//2. useEffect(callback,[])
//3. userEffect(callback,[deps])

//------------ Example useRef-----------------------
function TextInputWithFocusButton() {
  const inputEl = useRef(null);
  console.log(inputEl);
  const onButtonClick = () => {
    // `current` points to the mounted text input element
    inputEl.current.focus();
    console.log(inputEl.current.focus());
    console.log(inputEl);
  };
  return (
    <>
      <input ref={inputEl} type="text" />
      <button onClick={onButtonClick}>Focus the input</button>
    </>
  );
}
// export default TextInputWithFocusButton

//------- Example useRef get/set value----------

function App() {
  const prevValue = useRef("");
  const [value, setValue] = useState("");

  const onInputChange = (e) => {
    setValue(e.target.value);
  };

  const onInputBlur = () => {
    console.log(`Previous value: ${prevValue.current}`);
    console.log(`Current value: ${value}`);
    prevValue.current = value;
  };

  return (
    <div className="App">
      <input value={value} onBlur={onInputBlur} onChange={onInputChange} />
    </div>
  );
}
//export default App

// đếm số bằng state

function Count() {
  const [countter, setCounter] = useState(1);
  const handleIncrease = () => {
    setCounter(countter + 1);
  };
  return (
    <div>
      <h1>{countter}</h1>
      <button onClick={handleIncrease}>Increase</button>
    </div>
  );
}
//export default Count;

//random gift

const gifts = ["CPU i9", "RAM 32GB RGB", "RAM Keyboard"];

function Thuong() {
  const [gift, setGift] = useState();

  const randomGift = () => {
    const index = Math.floor(Math.random() * gifts.length);
    setGift(gifts[index]);
  };
  return (
    <div>
      <h1>{gift || "Chưa có phần thưởng"}</h1>
      <button onClick={randomGift}>Lấy thưởng</button>
    </div>
  );
}
//export default Thuong;

//two-way radio
// const courses = [
//   { id: 1, name: "html" },
//   { id: 2, name: "css" },
//   { id: 3, name: "javaScript" },
// ];
// function Twoway() {
//   const [checked, setChecked] = useState();
//   const [name, setName] = useState("");
//   console.log(checked);

//   const handleSubmit = () => {
//     console.log("hello");
//   };
//   return (
//     <div>
//       {courses.map((course) => (
//         <div key={course.id}>
//           <input
//             type="radio"
//             checked={checked === course.id}
//             onChange={() => setChecked(course.id)}
//           />
//           {course.name}
//         </div>
//       ))}
//       <button onClick={handleSubmit}>submit</button>
//     </div>
//   );
// }
//export default Twoway;

//checkbox
const courses = [
  { id: 1, name: "html" },
  { id: 2, name: "css" },
  { id: 3, name: "javaScript" },
];
function Twoway() {
  const [checked, setChecked] = useState([]);
  const [name, setName] = useState("");
  console.log(checked);

  const handleCheck = (id) => {
    setChecked((prev) => {
      const isChecked = checked.includes(id);
      if (isChecked) {
        return checked.filter((item) => item !== id);
      } else {
        return [...prev, id];
      }
    });
  };

  const handleSubmit = () => {
    console.log({ ids: checked });
  };

  return (
    <div>
      {courses.map((course) => (
        <div key={course.id}>
          <input
            type="checkbox"
            checked={checked.includes(course.id)}
            onChange={() => handleCheck(course.id)}
          />
          {course.name}
        </div>
      ))}
      <button onClick={handleSubmit}>submit</button>
    </div>
  );
}
//export default Twoway;
//todolist with useState

function Todo() {
  const [jobs, setJobs] = useState(() => {
    const storeJobs = JSON.parse(localStorage.getItem("jobs"));
    console.log(storeJobs);
    return storeJobs;
  });
  const [job, setJob] = useState("");

  const handleSubmit = () => {
    setJobs((prev) => {
      const newJobs = [...prev, job];
      const jsonJobs = JSON.stringify(newJobs);

      localStorage.setItem("jobs", jsonJobs);
      console.log(jsonJobs);
      return newJobs;
    });
    setJob("");
  };
  return (
    <div>
      <input value={job} onChange={(e) => setJob(e.target.value)} />
      <button onClick={handleSubmit}>Add</button>

      <ul>
        {jobs.map((job, index) => (
          <li key={index}>{job}</li>
        ))}
      </ul>
    </div>
  );
}
//export default Todo;

//mounted and unMounted thời điểm thêm và gỡ component kh dùng nữa

// 1, useEffect(callback)
// - gọi callback mỗi lần component re-render
//- gọi callback sau khi componenent thêm element vào dom
// 2, useEffect(callback, [])
//- chỉ gọi callback 1 lần sau khi component mounded
// 3, useEffect(callback, [deps])
//- callback sẽ được gọi lại mỗi khi deps thay đổi

//--------------
// 1. Callback luôn được gọi sau khi component mounted
//2. cleaup function luôn được gọi trươc khi component unmounted

const tabs = ["posts", "comments", "albums"];
function Mounted() {
  const [show, setShow] = useState(false);
  return (
    <div>
      <button onClick={() => setShow(!show)}>Show</button>
      {show && <Content />}
    </div>
  );
}
function Content() {
  const [title, setTitle] = useState("");
  const [posts, setPosts] = useState([]);
  const [type, setType] = useState("posts");
  const [showGoToTop, setShowGoToTop] = useState(false);

  //console.log(type);
  useEffect(() => {
    fetch(`https://jsonplaceholder.typicode.com/${type}`)
      .then((res) => res.json())
      .then((posts) => {
        setPosts(posts);
      });
  }, [type]);

  useEffect(() => {
    const handleScroll = () => {
      if (window.scrollY >= 200) {
        setShowGoToTop(true);
      } else {
        setShowGoToTop(false);
      }
      //setShowGoToTop(window.scrollY >= 200)
    };
    window.addEventListener("scroll", handleScroll);
    console.log("addEventListener");

    //cleaup fuction
    return () => {
      window.removeEventListener("scroll", handleScroll);
      console.log("removeEventListener");
    };
  }, []);
  return (
    <div>
      {tabs.map((tab) => (
        <button
          key={tab}
          style={
            type === tab
              ? {
                  color: "#fff",
                  background: "#333",
                }
              : {}
          }
          onClick={() => setType(tab)}
        >
          {tab}
        </button>
      ))}
      <input value={title} onChange={(e) => setTitle(e.target.value)} />
      <ul>
        {posts.map((post) => (
          <li key={post.id}>{post.title}</li>
        ))}
        {showGoToTop && (
          <button
            style={{
              position: "fixed",
              right: 20,
              bottom: 20,
            }}
          >
            GO TO TOP
          </button>
        )}
      </ul>
    </div>
  );
}
//render return trc r mới render useEffect
//export default Mounted;
//useRef

function Demo() {
  const [count, setCount] = useState(60);

  const timerId = useRef();
  const prevCount = useRef();

  useEffect(() => {
    prevCount.current = count;
  }, [count]);

  const handleStart = () => {
    timerId.current = setInterval(() => {
      setCount((prevCount) => prevCount - 1);
    }, 1000);
    console.log("start", timerId.current);
  };
  const handleStop = () => {
    clearInterval(timerId.current);

    console.log("stop", timerId.current);
  };
  console.log(count, prevCount.current);
  return (
    <div>
      <h1>{count}</h1>
      <button onClick={handleStart}>Start</button>
      <button onClick={handleStop}>Stop</button>
    </div>
  );
}
//export default Demo;

// useMemo
function UseMemo() {
  const [name, setName] = useState("");
  const [price, setPrice] = useState("");
  const [products, setProducts] = useState([]);

  const handleSubmit = () => {
    setProducts([
      ...products,
      {
        name,
        price: parseInt(price),
      },
    ]);
  };

  const total = useMemo(() => {
    const result = products.reduce((result, prod) => {
      console.log("tính toán lại...");
      return result + prod.price;
    }, 0);

    return result;
  }, [products]);
  return (
    <div>
      <input
        value={name}
        placeholder="enter name"
        onChange={(e) => setName(e.target.value)}
      />
      <input
        value={price}
        placeholder="enter price"
        onChange={(e) => setPrice(e.target.value)}
      />
      <button onClick={handleSubmit}>add</button>
      <br />
      total: {total}
      <ul>
        {products.map((product, index) => (
          <li key={index}>
            {product.name} - {product.price}
          </li>
        ))}
      </ul>
    </div>
  );
}

//export default UseMemo;

//UseReducer

//useState
// 1. Init state: 0
//2: action: up (state +1 ) / down(state + 2)

//useReducer
// 1. Init state: 0
//2: action: up (state +1 ) / down(state + 2)
//3. reducer
//4. dispatch

//Init state
const initState = 0;

//Action
const UP_ACTION = "up";
const DOWN_ACTION = "down";

//reducer
const reducer = (state, action) => {
  switch (action) {
    case UP_ACTION:
      return state + 1;
    case DOWN_ACTION:
      return state - 1;
    default:
      throw new Error("Invalid action");
  }
};
function UseReducer() {
  const [count, dispatch] = useReducer(reducer, initState);

  return (
    <div>
      <h1>{count}</h1>
      <button onClick={() => dispatch(DOWN_ACTION)}>Down</button>
      <button onClick={() => dispatch(UP_ACTION)}>Up</button>
    </div>
  );
}
//export default UseReducer;

//context
function Paragraph() {
  return (
    <div>
      <div>fffffffff</div>
    </div>
  );
}
function Contents() {
  return (
    <div>
      <Paragraph />
    </div>
  );
}
function Context() {
  return (
    <div>
      <button> Toggle theme</button>
      <Contents />
    </div>
  );
}
export default Context;
