import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './Hooks';
import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('root'));
const element = <App/>;
root.render(element);

reportWebVitals();
